import React from 'react';
import style from '../styles/prod-desc-nav.scss';

// add a reactjs classname utility.
var classNames = require('classnames');

class ProductDescriptionNav extends React.Component {

	constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    };

    // function to handle the click on prev and next button
    // params e = event; direction = string
    handleClick(direction){
            //e.preventDefault();

            var index = this.props.currentIndex;

    		switch(direction){
	    		case "next": 
                    // condition to know if the index is not in the last item
		    		if (index < this.props.numberOfItems - 1){
		    			index++;	
		    		}
		    		break;
	    		case "prev":
                    // condition to know if the index is higher that the first item
		    		if (index > 0){
		    			index--;
		    		}
		    		break; 
	    	}

            // call the function property which will handle the given index of the item to be displayed
    		this.props.onUserClick(index);
    };

    render() {

        // setup css class of the navigator links
         var navCSS = classNames({
            'collapsed': this.props.collapsed
         });

         // set next link title
         var nextTitle = this.props.nextTitle || false;

         // set previous link title
         var previousTitle = this.props.previousTitle || false;

        return (
    		<div className={"product-nav " + navCSS}>

                {/* only display the links if the item is greater than 1 */}
    			{ (this.props.numberOfItems > 1 && this.props.currentIndex > 0) && 
                    <button className="prev" title={previousTitle} href="#" onClick={() => this.handleClick('prev') }><span className="fa fa-caret-left"></span><span>Prev</span></button>
    			}
                { (this.props.numberOfItems > 1 && this.props.currentIndex < this.props.numberOfItems-1) && 
                    <button className="next" title={nextTitle} href="#" onClick={() => this.handleClick('next') }><span>{nextTitle}</span><span className="fa fa-caret-right"></span></button>
                }
                <div className="clear"></div>
    		</div>
        	
        );
    }
}

export default ProductDescriptionNav;