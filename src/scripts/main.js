import React from 'react';
import ReactDOM from 'react-dom';
import ProductWidget from './ProductWidget';

ReactDOM.render(<ProductWidget />, document.getElementById('widget_placeholder'));