import React from 'react';
import style from '../styles/prod-content-holder.scss';

var classNames = require('classnames');

class ProductContentHolder extends React.Component {

	constructor(props) {
        super(props);
    };

    render() {

        // check if there's data, if none then display the no data message.
        if (!this.props.data) {
            return (
                <p className="no-data">No Data.</p>
            );
        }

        var content = this.props.data;

        // setup css class for the content div
        // it will add "collapsed" class if the collapsed flag is true.
        var contentCSS = classNames({
            'collapsed': this.props.collapsed,
        });

        // setup css class for the description
        // it will add class "no-image" to the description so that it will display properly even without thumbnails.
        var descCSS = classNames({
            'no-image': !content.thumbnail
        });

        return (
    		<div className={"content-body " + contentCSS}>
                { /* only render the image holder if there's a thumbnail */ }
                { content.thumbnail ? <div className="image-holder"><img src={"../images/" + content.thumbnail} alt="" /></div> : null }
                <p className={"description " + descCSS } dangerouslySetInnerHTML={{__html: content.description}}></p>
            </div>
        
        );
    }
}

export default ProductContentHolder;