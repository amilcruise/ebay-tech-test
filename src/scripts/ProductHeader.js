import React from 'react';
import style from '../styles/prod-header.scss';

var classNames = require('classnames');

class ProductHeader extends React.Component {

	constructor(props) {
        super(props);
        this.handleCollapseClick = this.handleCollapseClick.bind(this);
    };


    handleCollapseClick(e){
        e.preventDefault();
        this.props.onCollapseToggleClick();
    }

    render() {
        var title = this.props.title;

        // setup class for the collapse button icon
        // DOWN if collapsed and UP if expanded
        var collapseButtonCSS = classNames({
            'fa-caret-down': this.props.collapsed,
            'fa-caret-up': !this.props.collapsed,
        });

        return (
    		<h1 className="widget-header">
                <span className="fa fa-file"></span>
                <span className="main-text">{title}</span>
                <a className="toggle-collapse" href="" onClick={this.handleCollapseClick} >
                    <span className={"fa " + collapseButtonCSS}></span>
                </a>
            </h1>
        
        );
    }
}

export default ProductHeader;