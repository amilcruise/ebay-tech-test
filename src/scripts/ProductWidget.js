import React from 'react';
// Load the json file
import content from 'json-loader!../../data/content.json';

import style from '../styles/prod-widget.scss';

// Load the components
import ProductHeader from './ProductHeader.js';
import ProductDescriptionNav from './ProductDescriptionNav.js';
import ProductContentHolder from './ProductContentHolder.js';

// add classname utility 
var classNames = require('classnames');

class ProductWidget extends React.Component {

	constructor(props) {
        super(props);
        // initialize states and functions
        this.state = content || [];
        this.state.currentIndex = 0;
        this.state.collapsed = false;
        this.showContent = this.showContent.bind(this);
        this.collapseToggle = this.collapseToggle.bind(this);
    };

    // function to set the current index of the item that will be displayed
    showContent(index){
    	this.setState({currentIndex:index});
    };

    // function to set the collapsed flag
    // will be invoked upon clicking of the collapse toggle button
    collapseToggle(){
    	var collapseToggle = !this.state.collapsed;
    	this.setState({collapsed: collapseToggle});
    }

    // function to get the next item's title 
    // accepts param index which is currentindex
    getNextTitle(index){
    	var title = "";
    	if (index < this.state.content.length - 1){
    		title = this.state.content[index + 1].title;
    	} else {
    		title = this.state.content[0] ? this.state.content[0].title : "";
    	}
    	return title;
    }

	// function to get the previous item's title 
    // accepts param index which is currentindex
    getPrevTitle(index){
    	var title = "";
    	if (index == 0 && this.state.content.length > 0){
    		title = this.state.content[this.state.content.length - 1].title;
    	} else {
    		title = this.state.content[index - 1] ? this.state.content[index - 1].title : "";
    	}
    	return title;
    }

    render() {
    	var currentIndex = this.state.currentIndex;
    	var collapsed = this.state.collapsed;

    	var current = this.state.content[currentIndex];
    	var nextTitle = this.getNextTitle(currentIndex);
    	var previousTitle = this.getPrevTitle(currentIndex);
    	var widgetCSS = classNames({
            'collapsed': this.state.collapsed
        });

        return (
        	<div className={"product-widget " + widgetCSS}>
        		<ProductHeader title={this.state.title} collapsed={collapsed} onCollapseToggleClick={this.collapseToggle}></ProductHeader>
				<ProductContentHolder collapsed={collapsed} data={current}></ProductContentHolder>
        		
                {  
                    // condition to only show the nav if there's more than one content item
                    this.state.content.length > 1 &&
                    <div>
                        <ProductDescriptionNav collapsed={collapsed} previousTitle={previousTitle} nextTitle={nextTitle} onUserClick={this.showContent} numberOfItems={this.state.content.length} currentIndex={currentIndex} ></ProductDescriptionNav>
                    </div>
                }
        	</div>
        );
    }
}

export default ProductWidget;