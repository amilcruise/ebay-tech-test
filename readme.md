Ebay (Gumtree) Tech Test
Jerameel Cruz

to download all npm dependencies launch terminal as admin and run: **npm install**

to run localhost: **npm start** (access using [http://localhost:8080](Link URL))

to build using webpack : **npm run build**

to see online running version: http://ebay-techtest-jerameel.surge.sh/